

export default function random() {
    const numbers = [];

    while (numbers.length < 5) {
        let number = randomInteger(5, 36);
        if (!numbers.includes(number)) {
            numbers.push(number);
        }
    }
    return numbers.sort((a, b) => a - b);
}

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}
