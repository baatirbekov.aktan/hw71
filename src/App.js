import React from "react";
import './App.css';
import Number from "./components/Number/Number";
import random from "./functions/Random/Random";
import Button from "@mui/material/Button";

class App extends React.Component {
    state = {
        numbers: {...random()}
    }

    changeNumber = () => {
        let numbers = this.state.numbers;
        numbers = {...random()};
        this.setState({numbers});
    }

    render() {
        let content = [];
        for (let i = 0; i < 5; i++) {
            content.push(<Number key={i} number={this.state.numbers[i]}/>);
        }
        return (
            <div className="App">
                <div className="button">
                    <Button size="large" color="success"  onClick={this.changeNumber} variant="outlined">New Numbers</Button>
                </div>
                <div className="numbers">
                    {content}
                </div>
            </div>
        );
    }
}

export default App;
